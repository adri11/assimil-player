var UPDATE_TIME_PASSED_INTERVAL = 1000,
    BACK_DELTA = 5,
    FORWARD_DELTA = BACK_DELTA;

var app = {
    activeMedia: null,
    activeFilePath: null,
    isPlaying: false,
    timer: null,
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        $('#js_play-pause').on('click', this.play.bind(this));
        $('#js_back').on('click', this.back.bind(this));
        $('#js_forward').on('click', this.forward.bind(this));
    },
    play: function() {
        if (this.isPlaying)
        {
            this.activeMedia.pause();
            this.isPlaying = false;
        }
        else
        {
            this.activeMedia.play();
            this.isPlaying = true;
        }
    },
    back: function() {
        this.activeMedia.getCurrentPosition(function(position){
            this.activeMedia.seekTo((position - BACK_DELTA)*1000);
        }.bind(this));
    },
    forward: function() {
        this.activeMedia.getCurrentPosition(function(position){
            this.activeMedia.seekTo((position + FORWARD_DELTA)*1000);
        }.bind(this));
    },
    hasMediaInstance: function() {
        return typeof this.activeMedia === 'Media';
    },
    onDeviceReady: function() {
        console.log('Device ready at '+new Date());
        this.checkIntent();
    },
    checkIntent: function (){
        window.plugins.webintent.getUri(this.loadMediaIfRequired.bind(this));
    },
    loadMediaIfRequired: function(filePath) {
        console.debug(filePath);
        console.trace();
        if (!_.isEmpty(filePath))
        {
            if (this.hasMediaInstance())
            {
                this.activeMedia.stop();
                this.stopTimer();
            }
            console.debug('media instance');
            this.activeMedia = new Media(filePath);
            this.onMediaLoaded(filePath);
        }
        console.log('File path: '+filePath);
    },
    onMediaLoaded: function(filePath)
    {
        this.activeMedia.play();
        setTitle(filePath);
        this.startTimer();
    },
    startTimer: function()
    {
        this.timer = setInterval(function()
            {
                this.activeMedia.getCurrentPosition(setTimePassed);
            }.bind(this),
            UPDATE_TIME_PASSED_INTERVAL
        );
    },
    stopTimer: function()
    {
        clearInterval(this.timer);
    }
};

function setTitle(title)
{
    $('#js_title').text(title);
}

function setTimePassed(timePassed)
{
    $('#js_time-passed').text(timePassed);
}

